import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
// import ListItemText from '@material-ui/core/ListItemText';
// import ListItem from '@material-ui/core/ListItem';
// import List from '@material-ui/core/List';
// import Divider from '@material-ui/core/Divider';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
// import IconButton from '@material-ui/core/IconButton';
// import Typography from '@material-ui/core/Typography';
// import CloseIcon from '@material-ui/icons/Close';
// import Slide from '@material-ui/core/Slide';
// import { TransitionProps } from '@material-ui/core/transitions';

import { getNews } from '../services/news';
import { rootState } from '../types/Root';
import store from '../app/store';

import Settings from '../components/Settings';
import AppHeader from '../components/AppHeader';
import NewsGrid from '../components//NewsGrid';
import AppBottomHeader from '../components/AppBottomHeader';

class Root extends React.Component<{}, rootState> {

    constructor(props: any) {
        super(props);
        this.state = {
            error: '',

        }
    }

    componentDidMount() {
        getNews();
        store.subscribe(() => {
            const state = store.getState();
            if (state.error !== null) this.setState({ error: state.error });
        });
    }

    closeErrorModal = () => {
        this.setState({ error: '' });
    }

    tryAgain = () => {
        this.closeErrorModal();
        getNews();
    }

    public render = () => {
        // const Transition = React.forwardRef<unknown, TransitionProps>(function Transition(props, ref) {
        //     return <Slide direction="up" ref={ref} {...props} />;
        // });

        return (
            <Router>
                <React.Fragment>
                    <AppHeader />
                    <Switch>
                        <Route path="/settings">
                            <Settings />
                        </Route>
                        <Route path="/">
                            <NewsGrid />
                        </Route>
                    </Switch>
                    <AppBottomHeader />
                    <Dialog
                        open={this.state.error !== ''}
                        onClose={() => this.closeErrorModal()}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">Failed to load news</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {this.state.error}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => this.tryAgain()} color="primary">Try again</Button>
                        </DialogActions>
                    </Dialog>
                    {/* <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                                <CloseIcon />
                            </IconButton>
                            <Typography variant="h6" className={classes.title}>
                                Sound
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <List>
                        <ListItem button>
                            <ListItemText primary="Phone ringtone" secondary="Titania" />
                        </ListItem>
                        <Divider />
                        <ListItem button>
                            <ListItemText primary="Default notification ringtone" secondary="Tethys" />
                        </ListItem>
                    </List>
                </Dialog> */}
                </React.Fragment>
            </Router>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        error: state.error
    };
};


export default connect(
    mapStateToProps,
)(Root);