
export interface ControlState {
    q: string,
    dateFrom: Date,
    dateTo: Date
}
export interface State {
    news: [],
    isLoading: boolean,
    page: number,
    error: any,
    q: string,
    dateFrom: Date,
    dateTo: Date
}