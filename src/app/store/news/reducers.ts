import { GET_NEWS, GET_NEWS_START, GET_NEWS_ERROR } from './types';

export const news = (state: any, action: any) => {
    switch (action.type) {
        case GET_NEWS:
            return { ...state, ...{ news: [...state.news, ...action.data.news.articles], isLoading: false, q: action.data.q, page: action.data.page + 1, dateFrom: action.data.from, dateTo: action.data.to } };
        case GET_NEWS_START:
            return { ...state, ...{ isLoading: true, news: action.page < state.page ? [] : state.news } };
        case GET_NEWS_ERROR:
            return { ...state, ...{ isLoading: false, error: action.error.message } };
        default:
            return state;
    }
}