import { getAllNews } from '../app/store/news/actions';
import store from '../app/store';

const getNews = (page = store.getState().page, from = store.getState().dateFrom, to = store.getState().dateTo, q = store.getState().q,) => {
    store.dispatch(getAllNews(page, from, to, q));
}

export {
    getNews
}