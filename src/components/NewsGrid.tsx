import React, { useEffect } from 'react';
import moment from 'moment';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

import newsGridClasses from '../styles/newsGrid';
import { News, NewsGridProps } from '../types/News';
import { connect } from 'react-redux';
import { getNews } from '../services/news';

const stateToProps = (state: NewsGridProps) => {
    return {
        news: state.news,
        isLoading: state.isLoading
    };
};

const NewsGrid = ({ news, isLoading }: NewsGridProps) => {
    const classes = newsGridClasses();

    const handleScroll = () => {
        if (window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight) return;
        getNews();
    }

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    return (
        <Box className={classes.root}>
            <Grid container spacing={3}>
                {news.map((item: News, i: number) => (
                    <Grid spacing={1} item xs={12} sm={3}>
                        <Card className={classes.card}>
                            <CardHeader
                                title={item.title}
                                subheader={moment(item.publishedAt).format('MMMM Do YYYY, h:mm:ss a')}
                            />
                            <CardMedia
                                className={classes.media}
                                image={item.urlToImage}
                                title={item.title}
                            />
                            <CardContent>
                                <Typography variant="body2" color="textSecondary" component="p">{item.description}</Typography>
                            </CardContent>
                            <CardActions disableSpacing>
                                <IconButton aria-label="share">
                                    <OpenInNewIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                ))}
            </Grid>
            {isLoading &&
                <CircularProgress className={classes.progress} />
            }
        </Box>
    );
}

export default connect(
    stateToProps
)(NewsGrid);