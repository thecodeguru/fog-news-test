import React from 'react';
import { connect } from 'react-redux';
import { getNews } from '../services/news';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';

import appHeaderClasses from '../styles/appHeader';

const stateToProps = (state: any) => {
    return {
        dateFrom: state.dateFrom,
        dateTo: state.dateTo,
        search: state.q
    };
};

const AppBottomHeader = ({ dateFrom, dateTo, q }: any) => {
    const classes = appHeaderClasses();

    return (
        <div className={classes.root}>
            <AppBar position="fixed" color="default" className={classes.bottomBar}>
                <Toolbar>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container justify="space-around">
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format="dd.MM.yyyy"
                                margin="normal"
                                id="date-picker-inline"
                                label="Date from"
                                value={dateFrom}
                                onChange={(date) => getNews(1, date, dateTo, q)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format="dd.MM.yyyy"
                                margin="normal"
                                id="date-picker-inline"
                                label="Date to"
                                value={dateTo}
                                onChange={(date) => getNews(1, date, date, q)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Grid>
                    </MuiPickersUtilsProvider>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default connect(
    stateToProps
)(AppBottomHeader);