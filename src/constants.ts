import Constants from './types/Constants';

const dateFrom = new Date();
dateFrom.setMonth(dateFrom.getMonth()-1);

export const sum
  = (...a: number[]) =>
    a.reduce((acc, val) => acc + val, 0);

export default {
    appName: 'Fog news',
    limit: 20,
    apiKey: '227fd70ee0864546ab94e9ad77a48041',
    initialState: {
        isLoading: true,
        news: [],
        page: 1,
        error: null,
        q: 'apple',
        dateFrom,
        dateTo: new Date(),
    }
} as Constants;