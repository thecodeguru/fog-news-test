import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import constants from './constants';
import App from './app/Root';
import store from './app/store/index';
import './styles/reset.css';

import * as serviceWorker from './serviceWorker';

document.title = constants.appName;

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

serviceWorker.register();
