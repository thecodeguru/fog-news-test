import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginTop: 124,
            paddingTop: 2,
            marginBottom: 94,
            paddingBottom: 0,
            paddingLeft: 20,
            paddingRight: 20,
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around',
            overflow: 'hidden',
            backgroundColor: theme.palette.background.paper,
        },
        card: {
            width: '100%',
            height: '100%'
        },
        media: {
            height: 0,
            paddingTop: '56.25%', // 16:9
        },
        expand: {
            transform: 'rotate(0deg)',
            marginLeft: 'auto',
            transition: theme.transitions.create('transform', {
                duration: theme.transitions.duration.shortest,
            }),
        },
        expandOpen: {
            transform: 'rotate(180deg)',
        },
        gridList: {
            width: '100%',
        },
        icon: {
            color: 'rgba(255, 255, 255, 0.54)',
        },
        progress: {
            margin: theme.spacing(2),
        },
    })
);